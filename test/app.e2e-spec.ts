import { Test, TestingModule } from '@nestjs/testing';
import { ExecutionContext, INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { ApiKeyGuard } from './../src/modules/auth/guards/api-key-guard';

describe('OrderController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).overrideGuard(ApiKeyGuard)
    .useValue({
      canActivate: (context: ExecutionContext) => {
        return true
      },
    })
    .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (POST)', () => {

    return request(app.getHttpServer())
      .post('/api/orders')
      .send({})
      .expect(400);
      });
});
