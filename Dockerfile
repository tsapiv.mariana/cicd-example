FROM node:16 AS builder
ENV NODE_ENV build
WORKDIR /app
COPY package*.json ./
COPY .env ./
COPY prisma ./prisma/
RUN npm install

COPY . .

RUN npm run build

FROM node:16 AS production
ENV NODE_ENV prod

FROM node:16

COPY --from=builder /app/node_modules ./node_modules
COPY --from=builder /app/package*.json ./
COPY --from=builder /app/.env ./
COPY --from=builder /app/dist ./dist

EXPOSE 3000
CMD [ "npm", "run", "start:prod" ]


