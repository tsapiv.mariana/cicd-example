-- CreateTable
CREATE TABLE "Item" (
    "productId" INT4 NOT NULL,
    "name" VARCHAR(20),
    "quantity" INT4,
    "weight" INT4,
    "eanCode" VARCHAR(20),
    "orderId" INT4
);

-- CreateTable
CREATE TABLE "Order" (
    "id" INT4 NOT NULL,
    "fullName" VARCHAR(20),
    "email" VARCHAR(20),
    "phone" VARCHAR(20),
    "addressLine1" VARCHAR(20),
    "addressLine2" VARCHAR(20),
    "company" VARCHAR(20),
    "zipCode" VARCHAR(20),
    "city" VARCHAR(20),
    "country" VARCHAR(20),
    "partnerCarrierKey" VARCHAR(20),
    "optCarrierKey" INT4,
    "status" VARCHAR(20),
    "created" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "Order_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Item_productId_key" ON "Item"("productId");

-- AddForeignKey
ALTER TABLE "Item" ADD CONSTRAINT "Item_orderId_fkey" FOREIGN KEY ("orderId") REFERENCES "Order"("id") ON DELETE SET NULL ON UPDATE CASCADE;
