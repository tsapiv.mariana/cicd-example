import { Body, Controller, Get, HttpCode, Post, UseGuards } from '@nestjs/common';

@Controller()
export class OrderController {

  @Get()
  @HttpCode(200)
  async ping(): Promise<any> {
    return 'Service is working';
  }
}
