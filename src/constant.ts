export const CARRIER_MAP = {
  DPD: 1001,
  DHL: 1002,
  'DHL Express': 1003,
  UPS: 1004,
  GLS: 1005,
};

export const CARRIER_LIST = ['DPD', 'DHL', 'DHL Express', 'UPS', 'GLS'];
