import { Module } from '@nestjs/common';
import { OrderService } from './services/order.service';
import { OrderController } from './order.controller';
import { AuthService } from '../auth/auth.service';
import { ConfigModule } from '@nestjs/config';
import { OrderValidationPipe } from './validation/order-validation.pipe';
import { OrderJobService } from './services/jobs/order-job.service';
import { OrderParserService } from './services/parsers/order.parser.service';
import { HttpModule } from '@nestjs/axios';
import { PartnerApiOrderService } from './services/partner-api-order.service';
import { OrderResolver } from './order.resolver';

@Module({
  imports: [ConfigModule, HttpModule],
  providers: [
    OrderService,
    AuthService,
    OrderValidationPipe,
    OrderJobService,
    OrderParserService,
    OrderResolver,
    PartnerApiOrderService,
  ],
  controllers: [OrderController],
})
export class OrderModule {}
