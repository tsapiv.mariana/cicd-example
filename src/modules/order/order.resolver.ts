import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { OrderService } from './services/order.service';
import { Order } from '../../types/graphql';

@Resolver('Order')
export class OrderResolver {
  constructor(private readonly orderService: OrderService) {}

  @Query('orders')
  getOrders(@Args('createdAfter') createdAfter: string) {
    return this.orderService.findCreatedAfter(createdAfter);
  }
}
