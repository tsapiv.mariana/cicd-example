import * as Joi from 'joi';
import { CARRIER_MAP } from '../../../constant';

export const DetailSchema = Joi.object({
  productId: Joi.number().required(),
  name: Joi.string().required(),
  quantity: Joi.number().required(),
  weight: Joi.number().required(),
  eanCode: Joi.string().required(),
});

export const OrderSchema = Joi.object({
  id: Joi.number().required(),
  fullName: Joi.string().required(),
  email: Joi.string().required(),
  phone: Joi.string().required(),
  addressLine1: Joi.string().required(),
  addressLine2: Joi.string().allow(null),
  company: Joi.string().allow(null),
  zipCode: Joi.string().required(),
  city: Joi.string().required(),
  country: Joi.string().required(),
  carrierKey: Joi.string()
    .valid(...Object.keys(CARRIER_MAP))
    .required(),
  status: Joi.string().required(),
  items: Joi.array().has(DetailSchema),
});
