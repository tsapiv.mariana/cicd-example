import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';
import { PrismaService } from '../../../prisma/prisma.service';
import { Status } from '../../../enum';
import { CreateOrderDto } from '../dto/order.dto';
import { OrderSchema } from './order-validation.schema';
import { Order } from '../interfaces/order.interface';
import { omit } from 'lodash';

@Injectable()
export class OrderValidationPipe implements PipeTransform<any> {
  constructor(private prisma: PrismaService) {}

  async transform(value: CreateOrderDto, metadata: ArgumentMetadata) {
    const mappedValue: Order = this.mapProperties(value);
    console.log(mappedValue);
    if (this.validateOrder(mappedValue)) {
      mappedValue.status = Status.NEW;
    } else {
      mappedValue.status = Status.ERROR;
    }
    return mappedValue;
  }

  validateOrder(order: Order) {
    const result = OrderSchema.validate(order);
    return !result.error;
  }

  mapProperties(value: CreateOrderDto): Order {
    const mappedOrder: Order = { ...value, items: value.details };
    delete mappedOrder['details'];
    return mappedOrder;
  }
}
