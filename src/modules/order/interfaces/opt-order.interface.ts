export interface OptOrder {
  OrderID: string;
  InvoiceSendLater: boolean;
  Issued: string;
  OrderType: string;
  Shipping: {
    CarrierID: number;
    DeliveryAddress: {
      AddressLine1: string;
      AddressLine2: string;
      City: string;
      Company: string;
      CountryCode: string;
      Email: string;
      PersonName: string;
      Phone: string;
      State: string;
      Zip: string;
    };
  };
  Products: {
    Barcode: string;
    OPTProductID: string;
    Qty: number;
    Weight: number;
  }[];
}
