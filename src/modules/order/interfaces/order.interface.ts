export interface Order {
  id?: number;
  fullName: string;
  email: string;
  phone: string;
  addressLine1: string;
  addressLine2: string;
  company: string;
  zipCode: string;
  city: string;
  country: string;
  carrierKey?: string;
  partnerCarrierKey?: string;
  optCarrierKey?: number;
  status: string;
  created?: Date;
  items?: Item[];
}

export interface Item {
  productId: number;
  name: string;
  quantity: number;
  weight: number;
  eanCode: string;
}
