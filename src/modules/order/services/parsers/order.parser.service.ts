import { Injectable } from '@nestjs/common';
import { Order } from '../../interfaces/order.interface';
import { OptOrder } from '../../interfaces/opt-order.interface';
import * as iso3311a2 from 'iso-3166-1-alpha-2';
@Injectable()
export class OrderParserService {
  public parse(order: Order): OptOrder {
    return {
      OrderID: order.id.toString(),
      InvoiceSendLater: false,
      Issued: order.created.toISOString(),
      OrderType: 'standard',
      Shipping: {
        CarrierID: order.optCarrierKey,
        DeliveryAddress: {
          AddressLine1: order.addressLine1,
          AddressLine2: order.addressLine2,
          City: order.city,
          Company: order.company,
          CountryCode: iso3311a2.getCode(order.country),
          Email: order.email,
          PersonName: order.fullName,
          Phone: order.phone,
          State: order.city,
          Zip: order.zipCode,
        },
      },
      Products: order.items.map((item) => {
        return {
          Barcode: item.eanCode,
          OPTProductID: item.eanCode,
          Qty: item.quantity,
          Weight: item.weight,
        };
      }),
    };
  }
}
