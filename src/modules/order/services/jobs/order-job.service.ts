import { Injectable, Logger } from '@nestjs/common';
import { CronJob } from 'cron';
import { SchedulerRegistry } from '@nestjs/schedule';
import { PartnerApiOrderService } from '../partner-api-order.service';
import { Order } from '../../interfaces/order.interface';
import { OptOrder } from '../../interfaces/opt-order.interface';
import { Status } from '../../../../enum';

@Injectable()
export class OrderJobService {
  constructor(
    private schedulerRegistry: SchedulerRegistry,
    private partnerService: PartnerApiOrderService,
  ) {}

  private readonly logger = new Logger(OrderJobService.name);

  async addCheckStatusJob(
    name: string,
    seconds: string,
    optOrder: OptOrder,
    order: Order,
  ) {
    const job = new CronJob(`${seconds} * * * * *`, async () => {
      this.logger.log(`Job ${name} has started`);
      await this.runJob(job, order.id);
    });
    this.logger.log(`Job ${name} has been created`);
    this.schedulerRegistry.addCronJob(name, job);
    job.start();
  }

  private async runJob(job: CronJob, id: number) {
  }

  private async updatePartnerApiOrderState(id: number) {
    await this.partnerService
      .updateOrderState(id, Status.FINISHED)
      .then((res) => {
        if (res.status == 200) {
          this.logger.log(`Finished order ${id} was sync with Partner API.`);
        } else {
          this.logger.log(
            `Finished order ${id} was not sync with Partner API.`,
          );
        }
      });
  }
}
