import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';
import { lastValueFrom } from 'rxjs';

@Injectable()
export class PartnerApiOrderService {
  constructor(
    private httpService: HttpService,
    private configService: ConfigService,
  ) {}

  public async updateOrderState(id: number, state: string) {
    return lastValueFrom(
      this.httpService.patch(
        this.configService.get('PARTNER_API') + id,
        { state: state },
        {
          headers: {
            'Content-Type': 'application/json',
            'X-API-KEY': this.configService.get('KEY2'),
          },
        },
      ),
    );
  }
}
