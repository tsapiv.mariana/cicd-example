import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { PrismaService } from '../../../prisma/prisma.service';
import { Order } from '../interfaces/order.interface';
import { Status } from '../../../enum';
import { OrderParserService } from './parsers/order.parser.service';
import { PartnerApiOrderService } from './partner-api-order.service';
import { OrderJobService } from './jobs/order-job.service';
import { CARRIER_MAP } from '../../../constant';

@Injectable()
export class OrderService {
  constructor(
    private prisma: PrismaService,
    private parser: OrderParserService,
    private partnerService: PartnerApiOrderService,
    private jobService: OrderJobService,
  ) {}

  private readonly logger = new Logger(OrderService.name);

  public async handleOrder(order: Order) {
    this.logger.log(`Trying to create order ${order.id}.`);
    const savedOrder = await this.checkAndCreate(order);
    if (savedOrder.status == Status.NEW) {
      const parsedOptOrder = this.parser.parse(savedOrder);
    }
  }

  async checkAndCreate(order: Order) {
    const exist = await this.count(order.id);
    if (exist == 0) {
      return this.create(order);
    } else {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: 'Order with this id already exists.',
        },
        HttpStatus.BAD_REQUEST,
      );
    }
  }

  create(order: Order) {
    return this.prisma.order.create({
      data: {
        id: order.id,
        fullName: order.fullName,
        email: order.email,
        phone: order.phone,
        addressLine1: order.addressLine1,
        addressLine2: order.addressLine2,
        company: order.company,
        zipCode: order.zipCode,
        city: order.city,
        partnerCarrierKey: order.carrierKey,
        optCarrierKey: CARRIER_MAP[order.carrierKey],
        country: order.country,
        status: order.status,
        items: {
          create: order.items,
        },
      },
      include: {
        items: true,
      },
    });
  }

  count(id: number) {
    return this.prisma.order.count({
      where: {
        id: id,
      },
    });
  }

  findAll() {
    return this.prisma.order.findMany();
  }

  findOne(id: number) {
    return this.prisma.order.findUnique({
      where: { id },
    });
  }

  findCreatedAfter(createdAfter: string) {
    return this.prisma.order.findMany({
      where: {
        created: {
          gte: new Date(createdAfter),
        },
      },
      include: {
        items: true,
      },
    });
  }

  update(id: number, order: Order) {
    return this.prisma.order.update({
      where: { id },
      data: order,
    });
  }

  remove(id: number) {
    return this.prisma.order.delete({
      where: { id },
    });
  }
}
