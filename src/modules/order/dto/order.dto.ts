export class OrderDto {
  id?: number;
  fullName: string;
  email: string;
  phone: string;
  addressLine1: string;
  addressLine2: string;
  company: string;
  zipCode: string;
  city: string;
  country: string;
  carrierKey: string;
  status: string;
  items: {
    productId: number;
    name: string;
    quantity: number;
    weight: number;
    eanCode: string;
  }[];
  created?: Date;
}

export class CreateOrderDto {
  id?: number;
  fullName: string;
  email: string;
  phone: string;
  addressLine1: string;
  addressLine2: string;
  company: string;
  zipCode: string;
  city: string;
  country: string;
  carrierKey: string;
  status: string;
  details: {
    productId: number;
    name: string;
    quantity: number;
    weight: number;
    eanCode: string;
  }[];
  created?: Date;
}
