import { Body, Controller, HttpCode, Post, UseGuards } from '@nestjs/common';
import { OrderService } from './services/order.service';
import { ApiKeyGuard } from '../auth/guards/api-key-guard';
import { OrderValidationPipe } from './validation/order-validation.pipe';
import { CreateOrderDto } from './dto/order.dto';

@Controller('/api/orders')
export class OrderController {
  constructor(private orderService: OrderService) {}

  @Post()
  @HttpCode(200)
  @UseGuards(ApiKeyGuard)
  async createOrder(@Body(OrderValidationPipe) order: CreateOrderDto): Promise<any> {
    await this.orderService.handleOrder(order);
    return;
  }
}
