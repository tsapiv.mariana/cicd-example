import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AuthService {
  constructor(private configService: ConfigService) {}

  async checkApiKey(key: string): Promise<boolean> {
    const savedKey = await this.configService.get('KEY1');
    return savedKey === key;
  }
}
