import { ConfigService } from '@nestjs/config';
import { Test } from '@nestjs/testing';
import { AuthService } from '../auth.service';

describe('AuthService', () => {
  let authService: AuthService;
  let configService: ConfigService;


  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
        providers: [ConfigService, AuthService],
      }).compile();

    configService = moduleRef.get<ConfigService>(ConfigService);
    authService = moduleRef.get<AuthService>(AuthService);
  });

  describe('checkApiKey', () => {
    it('should return true', async () => {
      const testKey = 'test';
      jest.spyOn(configService, 'get').mockImplementation(() => 'test');

      expect(await authService.checkApiKey(testKey)).toBe(true);
    });
  });
});