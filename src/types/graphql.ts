
/*
 * -------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */

export class Item {
    productId: number;
    name: string;
    quantity: number;
    weight: number;
    eanCode: string;
}

export class Order {
    id: number;
    fullName: string;
    email: string;
    phone: string;
    addressLine1: string;
    addressLine2?: Nullable<string>;
    company?: Nullable<string>;
    zipCode: string;
    city: string;
    country: string;
    partnerCarrierKey: string;
    optCarrierKey: string;
    status: string;
    items?: Nullable<Nullable<Item>[]>;
}

export abstract class IQuery {
    abstract orders(createdAfter: string): Nullable<Nullable<Order>[]> | Promise<Nullable<Nullable<Order>[]>>;
}

type Nullable<T> = T | null;
