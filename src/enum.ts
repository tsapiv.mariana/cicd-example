export enum Status {
  PENDING = 'Pending',
  NEW = 'New',
  INPRODUCTION = 'InProduction',
  FINISHED = 'Finished',
  ERROR = 'Error',
}
