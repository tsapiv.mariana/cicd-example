# OPT Task


## TODO
 - Add more validation
 - Add custom mapper for cities and countries
 - Can be used one job instead of creating separate one for checking status from OPT service
 - Custom exceptions

## Run
 - yarn
 - npm yarn start:dev

## Remote

App is accessible on http://159.223.176.92:3000/api/orders

